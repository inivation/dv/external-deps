#!/bin/sh

# Requirements: Ubuntu, GPG key (seahorse), devscripts, build-essential

# exit when any command fails
set -e

# Based upon Ubuntu Focal 22.04 LTS eigen3 package.

GPG_KEY_ID=
GPG_KEY_PASS=
PKG_NAME="eigen3"
PKG_VERSION="3.4.0"
PKG_RELEASE=1
DISTRO="bionic"
UPLOAD="false"
DEBUILD_ARGS="-S -sa -d -us -uc"

while test $# -gt 0
do
    case "$1" in
        --distro) DISTRO="$2"
            ;;
        --gpg-key-id) GPG_KEY_ID="$2"
            ;;
        --gpg-key-pass) GPG_KEY_PASS="$2"
            ;;
        --upload) UPLOAD="true"
            ;;
    esac
    shift
done

SRC_URI="https://gitlab.com/libeigen/eigen/-/archive/${PKG_VERSION}/eigen-${PKG_VERSION}.tar.gz"
PPA_REPO="inivation-ppa/inivation"

if [ "${DISTRO}" = "bionic" ] ; then
	PPA_REPO="inivation-ppa/inivation-bionic"
fi

DATE=$(LC_ALL=C date +'%a, %d %b %Y %T %z')
CUR_DIR=$(pwd)
BASE_DIR="$CUR_DIR/../../"
BUILD_DIR="$CUR_DIR/build/"
PKG_BUILD_DIR="$BUILD_DIR/eigen-$PKG_VERSION/"
DEBIAN_DIR="$PKG_BUILD_DIR/debian/"

echo "Started the debian source packaging process for distro $DISTRO"

rm -rf "$BUILD_DIR"
mkdir -p "$BUILD_DIR"

# Get original Ubuntu 22.04 eigen3 source tarballs
cd "$BUILD_DIR"
wget --no-verbose "${SRC_URI}" -O "${PKG_NAME}_${PKG_VERSION}.orig.tar.gz"
tar -xzf "${PKG_NAME}_${PKG_VERSION}.orig.tar.gz"

mkdir -p "$DEBIAN_DIR"

# Copy correct debian build files for distro
cp -R "$CUR_DIR/$DISTRO/"* "$DEBIAN_DIR/"

# Create the changelog file for the distro
CHANGELOG_FILE="$DEBIAN_DIR/changelog"
echo "$PKG_NAME ($PKG_VERSION-$PKG_RELEASE~$DISTRO) $DISTRO; urgency=low" > "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo "  * Released $PKG_NAME version $PKG_VERSION for distro $DISTRO." >> "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo " -- iniVation AG <support@inivation.com>  $DATE" >> "$CHANGELOG_FILE"

# Launch debuild
cd "$PKG_BUILD_DIR"
debuild $DEBUILD_ARGS

# Sign package
cd "$BUILD_DIR"
debsign -p"gpg --pinentry-mode loopback --passphrase $GPG_KEY_PASS" -S -k"$GPG_KEY_ID" \
  "${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}~${DISTRO}_source.changes"

# Send to Launchpad PPA
if [ "$UPLOAD" = "true" ]; then
	dput "ppa:$PPA_REPO" "${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}~${DISTRO}_source.changes"
fi;
