# external-deps

Track status of needed external dependencies, provide up-to-date/missing packages.

## Current status

![DV Dependencies](dv_deps_table.png)

## Notes

- Ubuntu Bionic (18.04 LTS): aravis 0.8.x cannot be built (requires newer meson tools).
  Disabled features: machine vision camera support.
- Windows / MSYS2: google-perftools not available for this platform.
  Disabled features: CPU/Heap profiler.
- Windows / VCPKG: libserialport and aravis not available on VCPKG for MSVC.
  Disabled features: eDVS support, machine vision camera support.
