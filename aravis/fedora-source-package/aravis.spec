%global majorversion 0.8

Name:		aravis
Version:	VERSION_REPLACE
Release:	0%{?dist}
Summary:	Aravis digital video camera acquisition library

Group:		Development/Libraries
License:	LGPLv2+
URL:		https://github.com/AravisProject/aravis
Source0:	https://github.com/AravisProject/aravis/releases/download/%{version}/aravis-%{version}.tar.xz

BuildRequires:	meson
BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	desktop-file-utils
BuildRequires:	intltool
BuildRequires:	gobject-introspection-devel
BuildRequires:	pkgconfig(glib-2.0) >= 2.26
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(libxml-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(libnotify)
BuildRequires:	pkgconfig(gstreamer-base-1.0) >= 1.0
BuildRequires:	pkgconfig(gstreamer-app-1.0)
BuildRequires:	pkgconfig(libusb-1.0)
BuildRequires:	pkgconfig(audit)

Requires:	glib2 >= 2.26
Requires:	libxml2

%description
Library for video acquisition using Genicam cameras (GigE and USB3).

%package devel
Summary:	Aravis digital video camera acquisition library -- Development files
Group:		Development/Libraries
Requires:	%{name} = %{version}
Requires:	pkgconfig(glib-2.0) >= 2.26
Requires:	pkgconfig(gobject-2.0)
Requires:	pkgconfig(gio-2.0)
Requires:	pkgconfig(libxml-2.0)
Requires:	pkgconfig(gthread-2.0)

%description devel
Library for video acquisition using Genicam cameras, development files.

%package viewer
Summary:	Aravis digital video camera acquisition library -- Viewer
Group:		Development/Libraries
Requires:	%{name} = %{version}
Requires:	libnotify
Requires:	gtk3
Requires:	gstreamer1-plugins-base
Requires:	gstreamer1-plugins-good
Requires:	gstreamer1-plugins-bad-free

%description viewer
Library for video acquisition using Genicam cameras, simple video viewer.

%prep
%setup -q -n aravis-%{version}

%build
%meson -Ddocumentation=disabled -Dgst-plugin=disabled
%meson_build

%install
%meson_install

mkdir -p %{buildroot}/lib/udev/rules.d/
cp src/aravis.rules %{buildroot}/lib/udev/rules.d/

desktop-file-install --vendor=""			\
       --dir=%{buildroot}%{_datadir}/applications/	\
       %{buildroot}%{_datadir}/applications/org.aravis.viewer-%{majorversion}.desktop

%post viewer
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%postun viewer
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%files
%{_bindir}/arv-tool-*
%{_bindir}/arv-fake-gv-camera-*
%{_bindir}/arv-camera-*
%{_bindir}/arv-test-*
%{_libdir}/lib%{name}-%{majorversion}*.so.*
%{_libdir}/girepository-1.0/*
%{_datarootdir}/locale
/lib/udev/rules.d/

%files devel
%{_includedir}/%{name}-%{majorversion}
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0/*
%{_libdir}/lib%{name}-%{majorversion}.so

%files viewer
%{_bindir}/arv-viewer-*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/applications/org.aravis.viewer-*.desktop
%{_datadir}/metainfo/org.aravis.viewer-*.metainfo.xml

%changelog
* Mon Apr 20 2020 iniVation AG <support@inivation.com> - VERSION_REPLACE
- New upstream release.
