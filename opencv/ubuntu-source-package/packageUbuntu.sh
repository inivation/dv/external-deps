#!/bin/sh

# Requirements: Ubuntu, GPG key (seahorse), devscripts, build-essential

# exit when any command fails
set -e

# Based upon Ubuntu Focal 20.04 LTS opencv package and patches from https://launchpad.net/~cran/+archive/ubuntu/opencv-4.2

GPG_KEY_ID=
GPG_KEY_PASS=
PKG_NAME="opencv"
PKG_VERSION="4.2.0+dfsg"
PKG_RELEASE=6
DISTRO="bionic"
UPLOAD="false"
DEBUILD_ARGS="-S -sa -d -us -uc"

while test $# -gt 0
do
   case "$1" in
        --gpg-key-id) GPG_KEY_ID="$2"
            ;;
        --gpg-key-pass) GPG_KEY_PASS="$2"
            ;;
        --upload) UPLOAD="true"
            ;;
    esac
    shift
done

PPA_REPO="inivation-ppa/inivation-bionic"

DATE=$(LC_ALL=C date +'%a, %d %b %Y %T %z')
CUR_DIR=$(pwd)
BASE_DIR="$CUR_DIR/../../"
BUILD_DIR="$CUR_DIR/build/"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION/"
DEBIAN_DIR="$PKG_BUILD_DIR/debian/"

echo "Started the debian source packaging process for distro $DISTRO"

rm -rf "$BUILD_DIR"
mkdir -p "$BUILD_DIR"

# Get original Ubuntu 20.04 opencv source tarballs
cd "$BUILD_DIR"
wget --no-verbose "http://archive.ubuntu.com/ubuntu/pool/universe/o/opencv/opencv_${PKG_VERSION}.orig.tar.xz"
tar -xJf "opencv_${PKG_VERSION}.orig.tar.xz"
wget --no-verbose "http://archive.ubuntu.com/ubuntu/pool/universe/o/opencv/opencv_${PKG_VERSION}.orig-contrib.tar.xz"
tar -xJf "opencv_${PKG_VERSION}.orig-contrib.tar.xz"
mv "opencv_contrib-4.2.0" "opencv-4.2.0/contrib"
mv "opencv-4.2.0" "$PKG_NAME-$PKG_VERSION"

mkdir -p "$DEBIAN_DIR"

# Copy correct debian build files for distro
cp -R "$CUR_DIR/$DISTRO/"* "$DEBIAN_DIR/"

# Create the changelog file for the distro
CHANGELOG_FILE="$DEBIAN_DIR/changelog"
echo "$PKG_NAME ($PKG_VERSION-$PKG_RELEASE~$DISTRO) $DISTRO; urgency=low" > "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo "  * Released $PKG_NAME version $PKG_VERSION for distro $DISTRO." >> "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo " -- iniVation AG <support@inivation.com>  $DATE" >> "$CHANGELOG_FILE"

# Launch debuild
cd "$PKG_BUILD_DIR"
debuild $DEBUILD_ARGS

# Sign package
cd "$BUILD_DIR"
debsign -p"gpg --pinentry-mode loopback --passphrase $GPG_KEY_PASS" -S -k"$GPG_KEY_ID" \
  "${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}~${DISTRO}_source.changes"

# Send to Launchpad PPA
if [ "$UPLOAD" = "true" ]; then
	dput "ppa:$PPA_REPO" "${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}~${DISTRO}_source.changes"
fi;
